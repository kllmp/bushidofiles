1. Las cosas que cargo son mis pensamientos. Son mi único peso. Mis pensamientos determinan si soy libre y ligero o pesado y agobiado
Kamal Ravikant. Rememorar constantemente las cosas es una carga muy pesada.

2. La meditación es el nuevo dispositivo móvil; se puede usar en cualquier lugar, en cualquier momento, discretamente
Sharon Salzberg. Es posible beneficiarse de la meditación en cualquier lugar y a cualquier hora.

3. Solo la mano que borra puede escribir lo verdadero
Meister Eckhart, una frase Zen para reflexionar sobre la verdad.

4. Aquellos que están libres del resentimiento encontrarán la paz seguro
Buddha, hablando sobre el odio y el resentimiento.

5. Preocupado por una sola hoja no verás el árbol
Si centras tu atención solamente en los aspectos negativos no verás los positivos.

6. Es curioso que la vida, cuanto más vacía, más pesa
Una reflexión sobre el desarrollo personal de cada uno de nosotros

7. En la mente del principiante hay muchas posibilidades, pero en la del experto hay pocas
La mente del principiante es una mente pura y no enjuiciadora. 



8. Al igual que los padres cuidan de sus hijos, deberías tener en cuenta a todo el universo
Zen Master Dogen, una frase sobre el cuidado de uno mismo.

9. Tres cosas no pueden seguir siendo escondidas: el sol, la luna y la verdad
Otra genial reflexión del grandísimo Buddha.

10. Si eres incapaz de encontrar la verdad justo donde estás, ¿dónde esperas encontrarla?
El presente es el único momento que podemos vivir.

11. Mi experiencia me dice que la mayoría de cosas no son tan malas como pensé que serían
Solemos preocuparnos en exceso por las cosas que nos ocurren.

12. Lo que podemos cambiar son nuestras percepciones, las cuales tienen el efecto de cambiar todo
Donna Quesada. No son los eventos lo que nos afecta, sino cómo los interpretamos.

13. Somos formados por nuestros pensamientos; nos convertimos en lo que pensamos.
Buddha. Somos lo que pensamos y actuamos en base a ello.

14. Recuerda que a veces no conseguir lo que quieres es un maravilloso golpe de suerte
Dalai Lama. No lograr lo que quieres puede ser una oportunidad de lograr otras cosas.

15. No eres tus pensamientos. Esta simple declaración pueden hacer un gran cambio en tu calidad de vida
Dawn Gluskin. Una frase Zen con cierto tono de ironía.

16. El Mindfulness no es complicado, solo tenemos que acordarnos de hacerlo
Sharon Salzberg, El mindfulness requiere actitud y práctica.

17. Una cosa: tienes que andar y crear el camino andando; no encontrarás un camino ya hecho. No es barato alcanzar la mayor realización de la verdad. Tendrás que crear el camino andando tu solo; el camino no esta ya hecho esperándote. Es justo como el cielo: los pájaros vuelan pero no dejan huellas. No los puedes seguir; no hay huellas detrás
Osho. Cada uno de nosotros debe labrarse el destino, porque el destino no está escrito.

18. No hay miedo para aquel cuya mente no está llena de deseos
Una reflexión que trata la relación entre los temores y los deseos.

19. El objeto de tu deseo no es un objeto
Una cita de Jack Gardner, que invita a pensar.

20. El amor verdadero nace de la comprensión
Según la filosofía Zen, amor solo se entiende con la comprensión

21. Mejor que mil palabras vacías, una sola palabra que pueda traer paz
En esta cita expone la importancia de una palabra llena de sentimiento.

22. Escapar del mundo significa que el mundo de uno no está preocupado con las opiniones del mundo
Uno no debe preocuparse por lo que los demás piensen de él..

23. La pureza y la impureza viene de uno mismo; nadie puede purificar a otro
Buddha resalta el papel central de la mente de cada uno

24. A un loco se le conoce por sus actos, a un sabio también
Somos lo que hacemos, es decir, las acciones que llevamos a cabo.

25. Con firmeza, entrénate para alcanzar la paz
Esta frase hace referencia a la búsqueda de paz interior.

26. Solo podemos perder aquello a lo que nos aferramos
No debemos apegarnos a nuestras emociones, sino observarlas y dejarlas pasar.

27. El estrés proviene de la forma en que te relacionas con eventos o situaciones
Chris Prentiss. Los eventos o las situaciones no nos estresan, sino cómo los interpretamos.

28. Llena tu mente con compasión
La compasión es uno de los principios de la filosofía Zen.

29. Todo lo que somos es el resultado de lo que hemos pensado
Buddha. Nuestros pensamiento determinan quiénes somos.

30. Cómo la gente te trata es su karma; cómo reaccionas es el tuyo
Wayne Dyer. Una cita sobre el Karma de este conocido autor.

31. No pensar sobre nada es Zen. Una vez sabes esto, caminar, sentarse o tumbarse, todo lo que haces es Zen
La filosofía Zen te envuelve y forma parte de tu día a día.

32. Es mejor viajar bien que llegar
Hay que vivir el presente para llegar al futuro.

33. Los problemas que permanecen persistentemente sin resolver, deberían ser tomados como preguntas cuestionadas de forma incorrecta
Alan Wilson Watts, sobre la resolución de problemas.

34. Cuanto más sabes, menos necesitas
Yvon Chouinard. La sabiduría te hace necesitar menos.



35. Si puedes traer tu consciencia, tu inteligencia al acto, si puedes ser espontáneo, entonces no hay necesidad de cualquier religión, la vida se vuelve en sí misma en la religión
Osho. Una cita sobre el autoconocimiento y sobre el presente y la espontaneidad.

36. No puedes viajar por el camino hasta que no te conviertes en el camino en sí mismo
Si no te centras en el presente y en tus sentidos no vas a avanzar.

37. El sentido de la vida espiritual es darse cuenta de la verdad. Pero nunca entenderás la vida espiritual, o la verdad, si la mides con tus propios criterios
Dainin Katagiri. Una interesante cita para reflexionar.

38. El poder de la mente es inconquistable
Séneca nos habla sobre el poder de la mente.

39. El estrés es un estado ignorante. Con el crees que todo es una emergencia. Nada es tan importante
Natalie Goldberg. El estrés es un problema que hace mella nuestros días.

40. La respuesta nunca es “ahí fuera”. Todas las respuestas son “ahí dentro”, dentro de ti, queriendo ser descubierta
Chris Prentiss. Una frase sobre el autoconocimiento y su importancia para ser feliz.

41. He vivido con muchos maestros Zen, muchos de ellos gatos
Eckhart Tolle. Una reflexión sobre los maestros Zen.

42. Todo ser humano es el autor de su propia salud o enfermedad
Otra cita de Buddha que hace referencia a la salud de cada uno.

43. Deberíamos vivir cada día como personas que acaban de ser rescatadas de la luna
Una gran frase Zen de Thích Nhất Hạnh.

44. Si quieres aprender, enseña. Si necesitas inspiración, inspira a otros. Si estas triste, anima a alguien

Una curiosa frase sobre la actitud.

45. Si quieres dominar la ansiedad de la vida, vive el momento, vive en la respiración
Una cita que hace clara referencia a la meditación budista.

46. El mejor truco de la mente es la ilusión de que existe
Marty Rubin. La mente es muy poderosa.

47. Para enseñar a los demás, primero has de hacer tú algo muy duro: has de enderezarte a ti mismo
Para poder enseñar, igual que para amar, primero debes entrar en contacto con tu propio yo.

48. Puedes ser feliz si estas dispuesto a dejar ir tu pasado y librarte de trabas para poder volar
El pasado pesa tanto que no te deja volar en la dirección que quieres.

49. No hay incendio como la pasión: no hay ningún mal como el odio
El odio es uno de los peores defectos del ser humano.

50. La presencia es cuando ya no esperas al siguiente momento, creyendo que el siguiente momento será más pleno que este
El presente es el único camino para vivir el mejor futuro.

51. Si añades un poco a lo poco, y lo haces con frecuencia, pronto poco llegará a ser mucho
La importancia de la constancia y la perseverancia resaltadas en esta cita.

52. Los sentimientos van y vienen como las nubes en un cielo ventoso. La respiración consciente es mi ancla
Thich Nhat Hanh, una de las grandes figuras del mindfulness, en clara referencia a la meditación.

53. La riqueza consiste mucho más en el disfrute que en la posesión
La filosofía Zen es totalmente opuesta al materialismo

54. Mejor una palabra que serene a quien la escucha que mil versos absurdos
En lo breve puede estar la clave.

55. La peor agresión a nosotros mismos, la peor de todas, es permanecer ignorante por no tener el valor y el respeto tratarnos a nosotros mismos con honestidad y ternura
Pema Chödrön. Tratarse a uno mismo con respeto es la clave del bienestar.



56. Camina como si estuvieras besando la tierra con tus pies
Otra célebre reflexión de Thich Nhat Hanh sobre vivir el aquí y ahora.

57. Más que mil palabras inútiles, vale una sola que otorgue paz
Las palabras que llevan a la calma son las más útiles.

58. No busques la amistad de quienes tienen el alma impura; no busques la compañía de hombres de alma perversa. Asóciate con quienes tienen el alma hermosa y buena. Si quieres ser feliz, rodéate de personas que valoren tu alma y que sepan comunicarse de una forma positiva.
Hay que rodearse de la gente con buen corazón.

59. Tu deber es descubrir tu mundo y después entregarte a él con todo tu corazón
Hay que vivir experiencias únicas porque nos hacen sabios.

60. Hay que ser conscientes de que lo que nos provoca malestar o ansiedad no son los eventos, sino como vinculamos las emociones a éstos
Los eventos no nos alteran, sino lo que pensamos de ellos y cómo los interpretamos.

61. Como una sólida roca no se mueve con el viento, así el sabio permanece imperturbado ante la calumnia y el halago
Las personas con experiencia y sabiduría tienen saben la importancia de quererse a uno mismo.

62. Lo que hoy somos descansa en lo que ayer pensamos, y nuestros actuales pensamientos forjan nuestra vida futura
El futuro ya no está y el pasado está por vivir. Lo que importa es el presente.

63. Estamos en este mundo para convivir en armonía. Quienes lo saben no luchan entre sí y alcanzan la paz interior
La paz en el mundo se consigue con la paz interior.

64. Larga es la noche para el que yace despierto; larga es la milla para el que va cansado; larga es la vida para el necio que no conoce la verdadera ley
Una frase que deja abierta la interpretación.

65. Con nuestros pensamientos creamos el mundo
El mundo que hemos creado no deja de ser un reflejo de nuestros pensamientos.

66. La máxima victoria es la que se gana sobre uno mismo
Lo primero que hay que hacer para conseguir el éxito personal es conocerse a uno mismo.

67. Por el esfuerzo, la vigilancia, la disciplina y el dominio de sí, el sabio se crea una isla que la inundación no logra destruir
Las personas sabias construyen el mundo a su alrededor en función de su experiencia.

68. Si tiene solución, ¿por qué lloras? Si no tiene solución, ¿por qué lloras?
No debemos malgastar esta preciosa vida lamentarnos por cosas que no tienen remedio.

69. Bendice a tu amigo... él te permite crecer
Quien tiene un amigo, tiene un tesoro dice un dicho popular.

70. Para vivir una vida desapegada, uno no se debe sentir dueño de nada en medio de la abundancia
La abundancia no tiene mucho sentido en la filosofía Zen.

71. Tu virtud debe ser profesar amor y paz a aquellos que te rodean
Dar paz y amor a los de tu alrededor te convierte en una persona virtuosa.

72. Así como una vela no brilla sin fuego, el hombre no puede existir sin una vida espiritual
La vida espiritual es conectar con nuestro alma.

73. Duda de todo. Encuentra tu propia luz
No prestes atención a lo que la sociedad quiere decirte. Sé crítico.

74. Conquistarse a uno mismo es una tarea más grande que conquistar a otros
Quererse a uno mismo es la mejor opción para ser feliz. 

75. La salida es a través de la puerta. ¿Por qué nadie utiliza este método?
Una de las frases zen de Confucio que se centran en una filosofía de vida basada en lo simple.

76. Aquellos que adoran no saben, y aquellos que saben no adoran
Uno de los aforismos del monje budista Bodhidharma.

77. Olvidarse de uno mismo es abrirse
Una breve reflexión de Dōgen, uno de lois grandes referentes históricos del budismo en Japón.

78. La vida es como prepararse para zarpar en un barco que terminará hundiéndose
Una comparación muy gráfica con la idea del recorrido vital de una persona.

79. Muchos caminos parten de la falda de la montaña, pero en su cima solo podemos mirar la luna
Una frase de Ikkyu, un monje zen del siglo XV.

80. Todo el mundo conoce el camino, pero pocos lo transitan
Otra de las frases de Bodhidharma acerca de lo simple que resulta vivir de forma virtuosa.

81. No veas las montañas desde la escala de los pensamientos humanos
La humildad y la consciencia de las propias limitaciones son esenciales en la vida.

82. El objetivo de la práctica siempre es mantener tu mente de principiante
Hay que mantener la frescura y la creatividad para desarrollarse correctamente.

83. Cuando alguien muere, esa es una gran lección
Una de las frases zen de Shunryu Suzuki, un monje y divulgador del budismo.

84. Prefiero ser derrotado en presencia de los sabiosa que destacar entre los ignorantes
Otra de las frases de Dōgen acerca de la importancia de rodearse de personas sabias.

85. Despojarse de las palabras es liberación
Bodhidharma habla sobre esa faceta psicológica que se encuentra más allá del lenguaje y que, según él, constituye la vida.

86. Uno debe ser muy consciente del fluir constante del mundo
Lo inmóvil no existe, es una ilusión.

87. El río que fluye a tu alrededor también fluye a mi alrededor
No existen personas aisladas, todos formamos parte de una misma realidad. Esta es una frase de Kabir, referente religioso de la India 

88. ¿Cuán incompleto está alguien que necesita tener muchas cosas?
Una reflexión de Sen no Rikyū acerca de la renuncia.

89. Nos sentamos juntos, la montaña y yo, hasta que solo la montaña permanece
Una poética frase de Li Bai, poeta chino.