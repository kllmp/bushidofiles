Manifiesto

Han cogido a otro hoy, esta en todos los diarios: "Adolescente arrestado por escandalo informatico", "Hacker arrestado por fraude bancario"... Malditos vagos, son todos iguales.

Pero alguna vez, en tu psicologia de tres piezas y tu techno-cerebro de  los años 50, has mirado a traves de los ojos de un hacker?

Te has preguntado alguna vez, que lo hace funcionar, que fuerzas lo han formado, que es lo que lo ha moldeado?

Soy un hacker, entra en mi mundo...

El mio es un mundo que comienza en la escuela... Soy mas listo que la mayoria de los otros niños, esta basura que nos enseñan me aburre... Malditos vagos, son todos iguales.

Estoy en el ultimo año o en la Universidad. He oido al profesor explicar  por decimoquinta vez, como reducir una fraccion. Lo entiendo. "No profesor, no hice el deber, lo tengo en mi cabeza..." Maldito vago, probablemente lo copio, son todos iguales.

Hoy hice un descubrimiento, encontro un ordenador. Espera un momento, esto es lo maximo, hace lo que yo quiero que haga. Si comete un error es por que yo la cague, no por que no le guste... O por que se sienta amenazado por mi... O por que piense que soy un sabelotodo... O no le gusta enseñarme y que no debera estar aqui... Maldito vago, todo lo que hace es jugar, son todos iguales.

Y entonces ocurrio... se abrio una puerta a un mundo... velozmente, a  traves de la linea telefonica, como heroina por las venas de un adicto, un pulso electrico es enviado fuera, un refugio contra la incompetencia diaria aparece... se encuentra una BBS. "Esto es,... esto es donde yo pertenezco...".

Conozco a todos aqui... incluso si no he estado con ellos nunca, o si no  he hablado con ellos nunca, o nunca vuelva a oir de ellos... Los conozco a todos... Maldito vago, ocupando la linea de telfono de nuevo, son todos iguales...

Puedes apostar tu trasero a que lo somos... nos han dado de comer en la boca comida de bebe, cuando nos moriamos por una buena chuleta... los pedazos de carne que nos hacian tragar, eran pedazos ya masticados y sin sabor.

Hemos sido dominados por sadicos, o ignorados por apaticos. Los pocos que tenian algo que enseñar, nos veian como estudiantes deseosos, pero esos son como gotas en el desierto. Este es nuestro mundo ahora... el mundo del electron y el interruptor, la belleza del baudio.

Hacemos uso de un servicio que ya existe, sin pagar por lo que podria y deberia ser tristemente barato o gratuito, si no fuese dirigido por glotones capitalistas, y tu nos llamas criminales.

Exploramos... y nos llamas criminales. Buscamos el conocimiento... y  nos llamas criminales. Existimos sin distinciones de piel, sin nacionalidad, sin influencias religiosas... y nos llamas criminales. Construyes bombas atomicas, libras guerras, engañas, nos mientes, y nos haces creer que es por nuestro propio bien, y seguimos siendo los criminales.

Pues si, soy un criminal. Mi crimen es el de la curiosidad. Mi crimen es el de juzgar a las personas por lo que dicen y piensan, no por su apariencia externa o lo grande que es su sueldo. Mi crimen es el de ser mas listo que tu, algo por lo que jamas me perdonaras. Soy un hacker, y este es mi manifiesto. Podras detener a este en concreto, pero no podras detenernos a todos... despues de todo, somos todos iguales.


Pero, incluso cuando escribo esto, empiezo a darme cuenta de por que somos un grupo de gente tan temido...

Somos incomprendidos por la mayoria... No puedes entender a alguien que juzga a los demas por lo que dicen, piensan y hacen, en vez de hacerlo por su apariencia externa o por lo grande que es su salario. No puedes entender a alguien que quiere ser honesto y generoso, en vez de mentir, robar y engañar. No puedes entendernos por que somos diferentes. Diferentes en una sociedad donde el conformismo es el estandar demandado.

Buscamos alzarnos por encima del resto, y despues, ayudar a subir a los demas a la misma nueva altura. Tratamos de innovar, de inventar.
Nosotros, seriamente tratamos de ir donde nadie ha ido antes. Somos incomprendidos, malinterpretados, desvirtuados. Todo porque simplemente queremos aprender.
Nosotros simplemente queremos aumentar el flujo de informacion y conocimiento, para que todos puedan aprender y beneficiarse.


Imaginemonos un mundo en el que la informacion fluye libremente...

Los problemas se deben resolver una sola vez. No se debe reinventar la
rueda...

Si todos tuvieramos a la mano como resolver nuestros problemas la vida
seria otra...

La verdad esta alla afuera...
